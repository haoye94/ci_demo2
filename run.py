import sys
from vunit   import VUnit, VUnitCLI

sys.argv.append("-v")

ui = VUnit.from_argv()
ui.add_osvvm()
# Libraries
Test = ui.add_library("Test")
Test.add_source_files("iir2.vhd")
Test.add_source_files("predictor_iir2.vhd")
Test.add_source_files("stub_sincos_adc.vhd")
# Testbench file
Test.add_source_files("testbench_iir2.vhd")

ui.set_sim_option("ghdl.sim_flags",["--wave=wave.ghw"])
ui.main()
