library IEEE;
    use IEEE.STD_LOGIC_1164.all;
    use ieee.math_real.all;
    use ieee.numeric_std.all;

entity stub_iir2 is
generic (
    RESOLUTION : natural := 16
);
port (
    iStart     : in std_logic;
    iEnable    : in std_logic;
    iData      : in integer;
    oData      : out integer;
    iB0        : in real;
    iB1        : in real;
    iB2        : in real;
    iA1        : in real;
    iA2        : in real
  );
end entity;

architecture rtl of stub_iir2 is

signal iData_sin           : real := 0.0;
signal iData_sin_prev      : real := 0.0;
signal iData_sin_prev_prev : real := 0.0;

signal oData_current       : real := 1.0;
signal oData_sin_prev      : real := 0.0;
signal oData_sin_prev_prev : real := 0.0;

begin
    oData <= integer(oData_current) when iEnable else iData;

    main : process
    begin
        wait until iStart = '1';
        iData_sin           <= real(iData);
        iData_sin_prev      <= iData_sin;
        iData_sin_prev_prev <= iData_sin_prev;
        oData_current       <= (iB0 * iData_sin) + (iB1 * iData_sin_prev) + (iB2 * iData_sin_prev_prev) - (iA1 * oData_sin_prev) - (iA2 * oData_sin_prev_prev);
        wait for 0 ns;
        oData_sin_prev      <= oData_current;
        oData_sin_prev_prev <= oData_sin_prev;
        wait for 0 ns;
        wait until iStart = '0';
    end process;
end architecture;
