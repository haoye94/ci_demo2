library ieee;
	use	ieee.std_logic_1164.all;
	use ieee.numeric_std.all;
	use ieee.math_real.all;

-- y0 = b0 * x0 + b1 * x1 + b2 * x2 - a1 * y1 - a2 * y2
entity Filter_IIR_2nd is
generic(
	DECIMAL_BITS: natural := 16
);
port(
	iClk		: in  std_logic;
	iEnable		: in  std_logic;
	iDataValid	: in  std_logic;
	oDataValid	: out std_logic;
	iB0			: in  signed(17 downto 0);
	iB1			: in  signed(17 downto 0);
	iB2			: in  signed(17 downto 0);
	iA1			: in  signed(17 downto 0);
	iA2			: in  signed(17 downto 0);
	iData		: in  signed(15 downto 0);
	oData		: out signed(15 downto 0)
);
end entity;

architecture rtl of Filter_IIR_2nd is

signal x_prev		: signed(iData'length - 1 downto 0) := (others => '0');
signal x_prev_prev	: signed(iData'length - 1 downto 0) := (others => '0');
signal y			: signed(oData'length - 1 downto 0) := (others => '0');
signal y_prev		: signed(oData'length - 1 downto 0) := (others => '0');
signal y_prev_prev	: signed(oData'length - 1 downto 0) := (others => '0');

type tState is (IDLE, B0, B1, B2, A1, A2, FINISHED);

signal state			: tState := IDLE;
signal mDataValidLatch 	: std_logic := '0';
signal mult_a			: std_logic_vector(15 downto 0);
signal mult_b			: std_logic_vector(17 downto 0);
signal mult_res			: signed(33 downto 0);
signal sum				: signed(mult_res'length + 3 - 1 downto 0);
signal reload_counter 	: unsigned(3 downto 0) := (others => '0');

begin
	mult_res <= signed(mult_a)*signed(mult_b);

	load_coeff_process: process(iClk)
	begin
		if rising_edge(iClk) then
			if state = IDLE then
				if iDataValid = '1' or mDataValidLatch = '1' then
					state			<= B0;
					mult_a 			<= std_logic_vector(iData);
					mult_b 			<= std_logic_vector(iB0);
					sum 			<= (others => '0');
					mDataValidLatch <= '0';
				end if;
			elsif state = B0 then
				sum 	<= sum + signed(mult_res);
				state 	<= B1;
				mult_a 	<= std_logic_vector(x_prev);
				mult_b 	<= std_logic_vector(iB1);
			elsif state = B1 then
				sum 	<= sum + signed(mult_res);
				state 	<= B2;
				mult_a 	<= std_logic_vector(x_prev_prev);
				mult_b 	<= std_logic_vector(iB2);
			elsif state = B2 then
				sum 	<= sum + signed(mult_res);
				state 	<= A1;
				mult_a 	<= std_logic_vector(y_prev);
				mult_b 	<= std_logic_vector(iA1);
			elsif state = A1 then
				sum 	<= sum - signed(mult_res);
				state 	<= A2;
				mult_a 	<= std_logic_vector(y_prev_prev);
				mult_b 	<= std_logic_vector(iA2);
			elsif state = A2 then
				sum 	<= sum - signed(mult_res);
				state 	<= FINISHED;
			elsif state = FINISHED then
				y 			<= shift_right(sum, DECIMAL_BITS)(oData'length - 1 downto 0);
				x_prev 		<= iData;
				x_prev_prev <= x_prev;
				y_prev 		<= shift_right(sum, DECIMAL_BITS)(oData'length - 1 downto 0);
				y_prev_prev <= y_prev;
				state 		<= IDLE;
			else
				state <= IDLE;
			end if;
		end if;
	end process;

	oDataValid <= iDataValid when iEnable = '0' else
						'1' when state = FINISHED else '0';
	oData <= y when iEnable = '1'else iData;

end architecture;
