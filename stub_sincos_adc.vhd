library ieee;
	use ieee.std_logic_1164.all;
	use ieee.float_pkg.all;
    use ieee.numeric_std.all;
	use ieee.math_real.all;

entity mock_sincos_adc is
generic (
    RESOLUTION              : natural := 16;
    SAMPLING_PERIOD         : real    := 1666.7; -- in nano seconds
    CYCLES_PER_REVOLUTION   : real    := 4096.0 -- sin cos cycles per turn
);
port (
    iAmplitude      : in real                           := 0.5;
    iSpeed          : in real                           := 10.0; -- rpm
    oSin            : out signed(RESOLUTION-1 downto 0) := to_signed(0,RESOLUTION);
    oCos            : out signed(RESOLUTION-1 downto 0) := to_signed(0,RESOLUTION);
    oSin_real       : out real                          := 0.0;
    oCos_real       : out real                          := 0.0
);
end entity;
architecture rtl of mock_sincos_adc is
constant maxValue           : real := 2.0 ** (RESOLUTION - 1); --note that siged adc output reduces resolution by half
signal mock_adc_sin         : real := 0.0;
signal mock_adc_cos         : real := 0.0;
signal amplitude            : real := 0.5;
signal freq                 : real := 2048.0;
signal t_ns                 : real := 0.0;
begin
    output : process
    begin
        freq            <= iSpeed/60.0*CYCLES_PER_REVOLUTION; -- convert from RPM to frequency
        wait for 0 ns;
        mock_adc_sin    <= iAmplitude * sin(2.0 * MATH_PI * freq * t_ns / 1000000000.0);
        mock_adc_cos    <= iAmplitude * cos(2.0 * MATH_PI * freq * t_ns / 1000000000.0);
        wait for 0 ns;
        oSin_real       <= mock_adc_sin;
        oCos_real       <= mock_adc_cos;
        oSin            <= to_signed(integer(round(mock_adc_sin * maxValue)),RESOLUTION);
        oCos            <= to_signed(integer(round(mock_adc_cos * maxValue)),RESOLUTION);
        t_ns            <= t_ns + SAMPLING_PERIOD;
        wait for SAMPLING_PERIOD*1 ns;
    end process;
end architecture;
